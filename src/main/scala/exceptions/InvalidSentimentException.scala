package exceptions


case class InvalidSentimentException(msg : String) extends RuntimeException(msg)

case class InvalidPickCountException(msg: String) extends RuntimeException(msg)
