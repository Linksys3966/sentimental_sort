package services

import exceptions.InvalidSentimentException
import scala.models.{State, Sentiment, Sentiments}

import scala.util._


class SentimentalSortService {

  def sortSentiments(bundledSentiments: Seq[String]): Seq[Sentiment] = {

    val sentiments: Sentiments         = Sentiments(bundledSentiments)
    val triedSentiments: Try[Seq[Int]] = sentiments.areValid

    val decoupledSentiments = triedSentiments.map(_.map(Sentiment)) match {
      case Success(sm)            => sm
      case Failure(ex: Exception) => throw new InvalidSentimentException(s"Something went wrong while parsing a sentiment : $ex")
    }

    val statesOfSentiments: Seq[State] = decoupledSentiments.map(_.state)
    val sentimentsWithStates           = decoupledSentiments zip statesOfSentiments
    val sentimentsWithDetachedState    = sentimentsWithStates.map{case(sm, state) => (sm.inverseNegative(state), state)}
    val sortedCentiments: Seq[(Sentiment, State)] = sentimentsWithDetachedState
                                                    .sortWith({case ((sentiment1, state1),(sentiment2, state2)) => sentiment2 lessThan sentiment1})

    sortedCentiments.map{case (sentiment, state) => sentiment inverseNegative state}
  }
}



