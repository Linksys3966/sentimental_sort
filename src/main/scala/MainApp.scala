import exceptions.InvalidPickCountException
import services.SentimentalSortService

import scala.util._

object MainApp {

   def main (args: Array[String]): Unit ={

     println("Enter sentiments separated by ,")
     val sentiments = readLine.split(",").toSeq
     println("Enter the pick count to pick top N sentiments as integer")
     val triedPickCount = Try(readInt)
     val pickCount = triedPickCount match {
       case Success(c)             => c
       case Failure(ex: Exception) => throw new InvalidPickCountException("Please enter a valid pick count")
     }
     println("Sentiments", sentiments)
     val sortedSentiments = new SentimentalSortService().sortSentiments(sentiments)
     val firstNSentiments = sortedSentiments.take(pickCount)
     println("Sorted Centiments", firstNSentiments)
  }

}
