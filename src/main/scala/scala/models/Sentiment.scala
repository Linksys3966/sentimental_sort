package scala.models

import scala.util.Try
import scala.wrappers.IntegerImplicits._


case class Sentiment(rating: Int) {
  
  def state                          = if (rating.isNegative) State.Negative else State.Positive
  def lessThan(sentiment: Sentiment) = this.rating < sentiment.rating
  
  def inverseNegative(state: State) = state match {
    case State.Negative => Sentiment(this.rating inverseNegative)
    case _              => this
  }

}

case class Sentiments(x: Seq[String]) {
  def areValid = {
    Try(x.map(_.toInt))
  }
}


object State{
  case object Positive extends State
  case object Negative extends State
}

trait State
