package scala.wrappers


object IntegerImplicits {

  implicit class RichInteger(val x: Int) {

    def isPositive = x > 0
    def isNegative = x < 0
    def inverseNegative = x * -1
    def toPositive = x * 1

  }

}
