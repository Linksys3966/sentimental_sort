package scala.models
import org.specs2.mutable.Specification

import scala.util.Failure

class SentimentTest extends Specification {

  "SentimentTest" should {

    "return the exact state of the sentiment" in {
      val negativeSentiment = Sentiment(-3)
      negativeSentiment.state mustEqual State.Negative
      val positiveSentiment = Sentiment(5)
      positiveSentiment.state mustEqual State.Positive
    }

    "compare sentiments" in {

      val sentiment1 = Sentiment(4)
      val sentiment2 = Sentiment(2)
      sentiment1 lessThan sentiment2 mustEqual false
    }


    "inverse negative sentiments based on the state" in {
      val sentimentWithNegativeState = Sentiment(-3)
      sentimentWithNegativeState.inverseNegative(State.Negative) mustEqual Sentiment(3)
    }

    "check validity of sentiments" in {
      val sentiments = Sentiments(Seq("-4", "3", "ee"))
      sentiments.areValid match {case Failure(_) => "" mustEqual "" case _ => "failure" mustEqual ""}
    }
  }
}
