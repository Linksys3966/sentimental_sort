package scala.wrappers

import org.specs2.mutable.Specification
import scala.wrappers.IntegerImplicits.RichInteger

class RichIntegerTest extends Specification {

  "RichIntegerTest" should {

    "return true depending on the value of the integer" in {
      val positiveValue = Int.box(2).toInt
      positiveValue.isPositive mustEqual true
      val negativeValue = Int.box(-4).toInt
      negativeValue.isNegative mustEqual true
    }

    "return the inverse of negative value" in {
      val negativeValue = Int.box(-3).toInt
      negativeValue.inverseNegative mustEqual 3
      val positiveValue = Int.box(2).toInt
      positiveValue.inverseNegative mustEqual -2
    }
  }

}
