package scala.services

import exceptions.InvalidSentimentException
import services.SentimentalSortService
import org.specs2.mutable.Specification

import scala.models.Sentiment

class SentimentalSortServiceTest extends Specification {
  
  "SentimentalSortServiceSpec" should {
    "sort sentiments" in {

      val sentimentalSortService = new SentimentalSortService()
      val sortedSentiments = sentimentalSortService.sortSentiments(Seq("2", "-1", "-5", "3", "4"))
      val expectedSentiments: Seq[Sentiment] = Seq(Sentiment(-5), Sentiment(4), Sentiment(3), Sentiment(2), Sentiment(-1))
      sortedSentiments mustEqual expectedSentiments
    }

    "throw InvalidSentimentException if any of the sentiment is invalid" in {

      val sentimentalSortService = new SentimentalSortService()
      sentimentalSortService.sortSentiments(Seq("2", "-rr", "-5", "3", "4")) must throwA[InvalidSentimentException]
    }
  }
}
